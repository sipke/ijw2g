.const KOALA_TEMPLATE = "C64FILE, Bitmap=$0000, ScreenRam=$1f40, ColorRam=$2328, BackgroundColor = $2710"
.var pic = LoadBinary("ijw2g.kla", KOALA_TEMPLATE)
.var bottomLeftSprites = LoadBinary("bottom_left_text - Sprites.bin")
.var bottomRightSprites = LoadBinary("bottom_right_text - Sprites.bin")
.var topSprites = LoadBinary("top_text - Sprites.bin")
.var closingSprites = LoadBinary("closing_words.bin")
.var lyricsSprites_0 = LoadBinary("lyrics_0 - Sprites.bin")
.var lyricsSprites_1 = LoadBinary("lyrics_1 - Sprites.bin")
.var lyricsSprites_2 = LoadBinary("lyrics_2 - Sprites.bin")

.var sid = LoadSid("lets_grump_no_filter_c000.sid")

.import source "macros.asm"

.const IRQ_LINE_0 = 20
.const IRQ_LINE_1 = 170

.const PIC_DEST = $6000
.const PIC_CHARS = $5c00

.const SPRITE_START = $5000

.const BOTTOM_D015 = %00111110
.const TOP_D015 = %01111110

.const PHASE_2_AT_X = 5
.const PHASE_3_AT_X = 7
.const PHASE_4_AT_X = 11
.const PHASE_5_AT_X = 19+6*8

.const COVER_SPRITE_POINTER = 0
.const COVER_SPRITE_COLOR = GREY
//                         look i just wnt to  grm  OK?  now fck of  music start
.var texttimer = List().add(50,30, 30, 30, 30, 50, 90,   50, 25, 100,(96*6))
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)
 //grp grp let all grp - - -
.eval texttimer.add( 24, 72, 18, 18, 57,1,1,1)

*=$0801
    BasicUpstart($080d)
*=$080d
    screenOff()
    jsr $e544
    writeText()

    copyPic()
    copySprites()
    copySid()
    setupSprites()
    clear8000Area()
    lda #0
    jsr $c000//sid.init
    jsr setupIrq

    jmp *
.import source "irq.asm"    
posx:
    .fill 6, 100+i*24
posy:
    .byte 50
    .byte 180  
timerCount:
    .byte 0
textTimerLo:     
    .fill texttimer.size(), <texttimer.get(i)
textTimerHi:    
    .fill texttimer.size(), >texttimer.get(i)
timer:
    .word 0    
dedicationText:
        // 1234567890123456789012345678901234567890
    .text " The character reminds me a bit of him  "    
    .text "  and he died while I was making this   "
    .text "   so I turned it into a dedication:    "
    .text "                                        "
    .text "          R.I.P. Sean Lock              "
coverSpriteX:
    .byte 132,146,178, 226, 0
    .byte 0, 0, 0,0
d015:
    //      look        i           just    want        to
    .byte %00000111, %00000111, %00011111, %01111111, %001111111        
//          grump       ok?         
    .byte %00001111, %00111111
    //      now     fuck        off!       //music start
    .byte %00000011, %00001111, %00111111, %00111111, 0
d015phase5:    
    //      grump   grump       let's       all         grump   (filler so i'll end up with 8 subphases)
    .byte %00000111, %00000111, %00011000, %00111000, %00111111, %00111111, %00111111,  %00111111
phase5SubPhase:
    .byte 0
seed:
    .byte $1d    
    .byte $1b    
phase5Y:
    .byte 141, 141, 140,140,140,140,140,140,140,140    
phase5X:
    .byte 100, 100, 110,110,110,110,110,110,110,110    

sidLoad: 
    .fill sid.size, sid.getData(i)
picLoad: 
    .fill pic.getSize(), pic.get(i)
spritesLoad:
    .fill topSprites.getSize(), topSprites.get(i)    
    .fill bottomLeftSprites.getSize(), bottomLeftSprites.get(i)    
    .fill bottomRightSprites.getSize(), bottomRightSprites.get(i)    
    .fill closingSprites.getSize(), closingSprites.get(i)    
    .fill lyricsSprites_0.getSize(), lyricsSprites_0.get(i)    
    .fill lyricsSprites_1.getSize(), lyricsSprites_1.get(i)    
    .fill lyricsSprites_2.getSize(), lyricsSprites_2.get(i)    
spritesLoadEnd: