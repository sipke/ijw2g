.macro copyPic(){
    ldy #ceil(8000/256)
    ldx #0
loop0:    
    lda picLoad,x
    sta PIC_DEST,x
    inx
    bne loop0
    inc loop0+2
    inc loop0+5
    dey
    bne loop0

    ldy #ceil(1000/256)
    ldx #0
loop1:    
    lda picLoad+8000,x
    sta PIC_CHARS,x
    lda picLoad+9000,x
    sta $d800,x
    inx
    bne loop1
    inc loop1+2
    inc loop1+5
    inc loop1+8
    inc loop1+11
    dey
    bne loop1

    lda picLoad+$2710
    sta $d021
    lda #GREY
    sta $d020
.break    
}


