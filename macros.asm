.macro screenOff(){
    lda $d011
    and #%11101111
    sta $d011
}
.macro screenOn(){
    lda $d011
    ora #%00010000
    sta $d011
}

.macro copySid() {
    .print "Sid block size:" + ceil(sid.size/256)
    ldy #ceil(sid.size/256)
    ldx #0
loop:    
    lda sidLoad,x
    sta sid.location,x

    .print "sid loc" + toHexString(sid.location)
    inx
    bne loop
    inc loop+2
    inc loop+5
    dey
    bne loop
}

.macro copyPic(){
    ldy #ceil(8000/256)
    ldx #0
loop0:    
    lda picLoad,x
    sta PIC_DEST,x
    inx
    bne loop0
    inc loop0+2
    inc loop0+5
    dey
    bne loop0

    ldy #ceil(1000/256)
    ldx #0
loop1:    
    lda picLoad+8000,x
    sta PIC_CHARS,x
    lda picLoad+9000,x
    sta $d800,x
    inx
    bne loop1
    inc loop1+2
    inc loop1+5
    inc loop1+8
    inc loop1+11
    dey
    bne loop1

    lda picLoad+$2710
    sta $d021
    lda #GREY
    sta $d020

}
.macro    copySprites(){
    ldy #ceil((spritesLoadEnd-spritesLoad)/256)
    ldx #0
loop:    
    lda spritesLoad,x
    sta SPRITE_START,x
    sta $8000,x
    inx
    bne loop
    inc loop+2
    inc loop+5
    inc loop+8
    dey
    bne loop
//cover sprite
    ldx #63 
    lda #$ff
!:  sta $4000,x
    dex
    bpl !-
}

.macro setupSprites() {
    ldx #0
    ldy #0
!:  lda #0
    sta $d028,x
    lda posx,x
    sta $d002,y
    lda posy
    sta $d003,y
    txa
    clc
    adc #(SPRITE_START& $3fff)/64
    sta PIC_CHARS+$03f9,x
    inx
    iny
    iny
    cpx #7
    bne !-
    lda #0
    sta $d01c

    lda #%00000001
    sta $d01d //x xpand
    lda #COVER_SPRITE_COLOR
    sta $d027

    lda #TOP_D015
    sta $d015
    lda $dd00
    and #%11111100
    ora #%00000010
    sta $dd00
    lda #%01111001
    sta $d018

    lda $d016
    ora #%00010000
    sta $d016

    lda #COVER_SPRITE_POINTER
    sta PIC_CHARS+$03f8
}
.macro setVICMemTo8000(){

    screenOff()
    lda $dd00
    and #%11111100
    ora #%00000001
    sta $dd00
    lda #$8f
    sta $d018
    lda $d016
    and #%11101111
    sta $d016

    .for(var i=0; i<6; i++) {
        lda #i+10+64
        sta PIC_CHARS+$4000+ $07f8+i
        sty $d003+i*2
    }
    screenOn()
}
.macro clear8000Area(){
    ldy #32
    ldx #0
    lda #$cc
!:    
    sta $a000,x
    inx
    bne !-
    inc !- +2
    dey
    bne !-
   

}
.macro setYAndPointersPhase1And2(){
        ldy #60
    sty $d001
    .for(var i=0; i<6; i++) {
        lda #i+64
        sta PIC_CHARS+$03f9+i
        lda #100+i*24
        sta $d002+i*2
        sty $d003+i*2
    }
}
.macro setYAndPointersPhase3(){
    ldy #60
    sty $d001
    .for(var i=0; i<6; i++) {
        lda #i+64
        sta PIC_CHARS+$03f9+i
        lda #100+i*24
        sta $d002+i*2
        sty $d003+i*2
    }
}

.macro setYAndPointersPhase4(){
    ldy #140
    // sty $d001
    .for(var i=0; i<6; i++) {
        lda #i+11
        sta $a3f8+i
        lda #100+i*24
        sta $d000+i*2
        sty $d001+i*2
    }
}
.macro setYAndPointersPhase5(){

    lda #0
    sta $d01d
    sta $d027
    ldy phase5SubPhase
    lda d015phase5,y
    sta $d015

    lda phase5Y,y
    .for(var i=0; i<6; i++) {
        sta $d001+i*2
    }

    lda #110
    sta $d006
    lda #134
    sta $d008
    lda #144
    sta $d00a
    lda #161
    sta $d000
    lda #185
    sta $d002
    lda #209
    sta $d004
    
    lda #20
    sta $a3f8
    lda #21
    sta $a3f9
    lda #22
    sta $a3fa
    lda #17
    sta $a3fb
    lda #18
    sta $a3fc
    lda #19
    sta $a3fd

    cpy #2
    bcs !+
    lda phase5X
    sta $d000
    clc
    adc #24
    sta $d002
    clc
    adc #24
    sta $d004
!:    



    //randomize
    lda seed
    beq doEor
    clc
    asl
    beq noEor    //if the input was $80, skip the EOR
    bcc noEor
doEor:
    eor #$1d
noEor:
    sta seed
    //randomize
    lda seed+1
    beq doEor2
    clc
    asl
    beq noEor2    //if the input was $80, skip the EOR
    bcc noEor2
doEor2:
    eor #$1d
noEor2:
    sta seed+1

}
.macro writeText(){
    ldx #0
!:  lda dedicationText,X
    sta $0400 + 400,x
    inx
    cpx #200
    bne !-
}
.macro setTextScreenAt0400(){
    screenOff()
    lda $dd00
    ora #%00000011
    sta $dd00
    lda #$17
    sta $d018
    lda $d016
    and #%11011111
    sta $d016
    lda $d011
    and #%11011111
    sta $d011
    lda #3
    ldx #0
!:  sta $d800,x
    sta $d900,x
    sta $da00,x
    sta $db00,x
    inx
    bne !-
    screenOn()

}
