setupIrq:
    sei


    lda #$7f
    sta $dc0d
    sta $dd0d

    lda $dc0d
    lda $dd0d

    lda #<irq
    sta $fffe//$0314
    lda #>irq
    sta $ffff//$0315
    lda #<IRQ_LINE_0
    sta $d012
    
    lda #((>IRQ_LINE_0)<<7) | $3b
    sta $d011

    lda #1
    sta $d01a

    lda #$35
    sta $01

    cli

    lda textTimerLo
    sta timer
    lda textTimerHi
    sta timer+1


    rts
//-------------------------------------------------    
irq:
    pha
    txa
    pha
    tya
    pha

    ldx timerCount
    cpx #PHASE_2_AT_X
    bcs !+
    jmp phase1_top
!:
    cpx #PHASE_2_AT_X
    bcs !+
    jmp phase2_top
!:  cpx #PHASE_3_AT_X
    bcs !+ 
    jmp phase3_top
!:  cpx #PHASE_4_AT_X
    bcs !+
    jmp phase4_top
!:  cpx #PHASE_5_AT_X
    bcs !+
    jmp phase5_top
!:
phase6top:
    lda #0
    sta $d015
    jmp spritesDone0    
phase5_top:
!:  setYAndPointersPhase5()
    jmp spritesDone0    
phase4_top:
    setYAndPointersPhase4()

    //lda coverSpriteX,x
    //sta $d000
    lda d015,x
    sta $d015
    jmp spritesDone0
phase3_top:
    setYAndPointersPhase3()
    lda #0
    sta $d000
    lda #TOP_D015
    sta $d015
    jmp spritesDone0
phase2_top:
    setYAndPointersPhase1And2()
    lda #0
    sta $d000
    lda #TOP_D015
    sta $d015
    jmp spritesDone0
phase1_top:
    setYAndPointersPhase1And2()
    lda coverSpriteX,x
    sta $d000

    lda d015,x
    sta $d015
spritesDone0:    
    ldx timerCount
    cpx #PHASE_4_AT_X-1
    bne !+
    jsr $c003//sid.play
!:    
    cpx #PHASE_4_AT_X
    bcc !+
    jsr $c003//sid.play
!:
//---------------- wait for line to redraw sprites
    lda #IRQ_LINE_1
!:  cmp $d012
    bne !-

//------------------------------
//if not yet phase 2, don't draw second row of sprites
    ldx timerCount
    cpx #PHASE_2_AT_X
    bcs !+
    jmp spritesDone1
!:  cpx #PHASE_3_AT_X
    bcc !+
    //beyond phase 3 done
    jmp spritesDone1
!:    
// re-place sprites
    ldy #230
    sty $d001
    .for(var i=0; i<5; i++) {
        lda #i+6+64
        sta PIC_CHARS+$03f9+i
        sty $d003+i*2
    }
    lda #120
    sta $d002
    lda #144
    sta $d004
    lda #168
    sta $d006
    
    lda #168
    sta $d008
    lda #192
    sta $d00a


//phase 3 reached?
    
    cpx #PHASE_3_AT_X
    bcc phase2
    cpx #PHASE_4_AT_X
    bcc phase3
//phase 4
    lda #0
    sta $d015
    jmp spritesDone1
phase3:
    lda #0
    sta $d015
    jmp spritesDone1
phase2:
    lda coverSpriteX,x
    sta $d000
    lda d015,x
    sta $d015

spritesDone1:
    ldx timerCount
    cpx #PHASE_4_AT_X
    bcc !+
!:

    dec timer
    bne !+

    lda timer+1
    beq nextTimer
    dec timer+1
!:    
exit:
    lda #$ff
    sta $d019

    pla 
    tay
    pla
    tax
    pla
    rti
nextTimer:
{
    ldx timerCount
    inx
    stx timerCount
    lda textTimerLo,x
    sta timer
    lda textTimerHi,x
    sta timer+1
    cpx #PHASE_3_AT_X
    bne !+

    setVICMemTo8000()
!:  cpx #PHASE_3_AT_X
    bne !+
    lda #0 
    sta $d01d
    sta $d027
!:  cpx #PHASE_5_AT_X
    bne !+
    setTextScreenAt0400()
    lda #0
    sta $d015

!:  cpx #PHASE_4_AT_X
    bcc return
    txa
    sec
    sbc #PHASE_4_AT_X
    and #7
    sta phase5SubPhase

    lda seed
    tay
    and #63
    adc #69
    sta phase5Y
    lda seed+1
    and #63
    adc #71
    sta phase5Y+1
    tya
    and #127
    adc #51
    sta phase5X
    lda seed+1
    and #127
    adc #51
    sta phase5X+1

return:    
    jmp exit
}