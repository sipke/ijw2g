setupIrq:
    sei


    lda #$7f
    sta $dc0d
    sta $dd0d

    lda $dc0d
    lda $dd0d

    lda #<irq
    sta $fffe//$0314
    lda #>irq
    sta $ffff//$0315
    lda #<IRQ_LINE_0
    sta $d012
    
    lda #((>IRQ_LINE_0)<<7) | $1b
    sta $d011

    lda #1
    sta $d01a

    lda #$35
    sta $01

    cli

    rts
//-------------------------------------------------    
irq:
    pha
    txa
    pha
    tya
    pha
    inc $d020

    jsr $c003//sid.play

    dec $d020
//---------------- wait for line to redraw sprites
    lda #IRQ_LINE_1
    
!:  cmp $d012
    bne !-
    inc $d020
    jsr $c003//sid.play
    dec $d020
exit:
    lda #$ff
    sta $d019    
    pla 
    tay
    pla
    tax
    pla
    rti
