.const KOALA_TEMPLATE = "C64FILE, Bitmap=$0000, ScreenRam=$1f40, ColorRam=$2328, BackgroundColor = $2710"
.var pic = LoadBinary("ijw2g.kla", KOALA_TEMPLATE)


.import source "macros2.asm"

.const IRQ_LINE_0 = 20
.const IRQ_LINE_1 = 170

.const PIC_DEST = $6000
.const PIC_CHARS = $5c00

.const SPRITE_START = $5000

.const BOTTOM_D015 = %00111110
.const TOP_D015 = %01111110

.const PHASE_2_AT_X = 5
.const PHASE_3_AT_X = 7
.const PHASE_4_AT_X = 10
.const PHASE_5_AT_X = 13

.const COVER_SPRITE_POINTER = 0
.const COVER_SPRITE_COLOR = GREY

*=$0801
    BasicUpstart($080d)
*=$080d
//    copySid()
    lda #0
//    jsr $c000

    copyPic()
    lda $dd00
    and #%11111100
    ora #%00000010
    sta $dd00
    lda #%01111001
    sta $d018

    lda $d016
    ora #%00010000
    sta $d016
    
    //jsr setupIrq

    jmp *
.import source "irq2.asm"    
posx:
    .fill 6, 100+i*24
posy:
    .byte 50
    .byte 180  
// sidLoad: 
//     .fill sid.size, sid.getData(i)
picLoad: 
    .fill pic.getSize(), pic.get(i)
